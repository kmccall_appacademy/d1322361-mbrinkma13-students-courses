class Student
  def initialize(first_name, last_name)
    @first_name = first_name
    @last_name = last_name
    @courses = []
  end

  attr_reader :first_name, :last_name, :courses

  def name
    @first_name + ' ' + @last_name
  end

  def enroll(new_course)
    if has_conflict?(new_course) == false && not_enrolled?(new_course)
      @courses.push(new_course)
    end
    if new_course.students.include?(self) == false
      new_course.students.push(self)
    end
  end

  def course_load
    course_load = Hash.new(0)
    @courses.each do |course|
      course_load[course.department] += course.credits
    end
    course_load
  end

  def has_conflict?(new_course)
    self.courses.each do |course|
      if course.conflicts_with?(new_course)
        raise "#{course} conflicts with #{new_course}!"
      end
    end
    false
  end

  def not_enrolled?(new_course)
    if @courses.include?(new_course)
      false
    else
      true
    end
  end

end
